#!/usr/bin/env python
# coding: utf-8

# In[41]:


# import numpy as np
# import pandas as pd


# In[42]:


# datapath = "./toy_dataset.csv"
# data1 = pd.read_csv(datapath,sep=",")
# data1.shape


# In[43]:


# import pandas as pd
# import numpy as np
# from sklearn import preprocessing

# le_city = preprocessing.LabelEncoder()
# tmp=["City","Gender","Illness"]
# les={"City":le_city}

# final_data = data1.copy()
# for x in tmp:
#     les[x] = preprocessing.LabelEncoder()
#     final_data[x]=les[x].fit_transform(data1[x])


# In[44]:


# final_data.head()


# In[45]:


# 결측치 확인

#final_data.isnull().sum()

# 결측치 없음


# In[3]:


# 데이터 형태 파악

# final_data.info()


# In[5]:


# 데이터 imbalance 확인

# import pandas as pd

# pd.crosstab(final_data["Income"],final_data.Gender,margins=True)

# gender feature는 불균형 나타나지 않음


# In[7]:


# pd.crosstab(final_data["Income"],final_data.Illness,margins=True)

# Illness feature는 불균형


# In[9]:


# pd.crosstab(final_data["Income"],final_data.Age,margins=True)

# Age feature는 불균형 나타나지 않음


# In[50]:


# 정규화 한 것


# from sklearn.model_selection import KFold
# from sklearn.linear_model import LinearRegression
# from sklearn.preprocessing import MinMaxScaler

# features = ["City","Gender","Illness","Age"]

# kf = KFold(n_splits=10,shuffle=True)
# accrs = []
# fold_idx = 1

# scaler = MinMaxScaler(feature_range=(0,1))

# for train_idx,test_idx in kf.split(final_data):
#     print("fold {}".format(fold_idx))
#     train_d, test_d = final_data.iloc[train_idx],final_data.iloc[test_idx]
    
#     train_y = train_d["Income"]
#     train_x = train_d[features]
#     train_x = scaler.fit_transform(train_x)

#     test_y = test_d["Income"]
#     test_x = test_d[features]
#     test_x = scaler.transform(test_x)

#     models = LinearRegression()
#     models.fit(train_x,train_y)
#     print("model {} = {}".format(models,models.score(test_x,test_y)))
    
#     mean_accr = models.score(test_x,test_y)
#     accrs.append(mean_accr)
    
#     fold_idx+=1
    
# print("average mae = {}".format(np.average(accrs)))


# In[28]:


# #전체 feature 선택


# import numpy as np
# import pandas as pd

# datapath = "./toy_dataset.csv"
# data1 = pd.read_csv(datapath,sep=",")
# data1.shape

# import pandas as pd
# import numpy as np
# from sklearn import preprocessing

# le_city = preprocessing.LabelEncoder()
# tmp=["City","Gender","Illness"]
# les={"City":le_city}

# final_data = data1.copy()
# for x in tmp:
#     les[x] = preprocessing.LabelEncoder()
#     final_data[x]=les[x].fit_transform(data1[x])

# from sklearn.model_selection import KFold
# from sklearn.linear_model import LinearRegression

# features = ["City","Gender","Illness","Age"]

# kf = KFold(n_splits=10,shuffle=True)
# accrs = []
# fold_idx = 1

# for train_idx,test_idx in kf.split(final_data):
#     print("fold {}".format(fold_idx))
#     train_d, test_d = final_data.iloc[train_idx],final_data.iloc[test_idx]
    
#     train_y = train_d["Income"]
#     train_x = train_d[features]

#     test_y = test_d["Income"]
#     test_x = test_d[features]

#     models = LinearRegression()
#     models.fit(train_x,train_y)
#     print("model {} = {}".format(models,models.score(test_x,test_y)))
    
#     mean_accr = models.score(test_x,test_y)
#     accrs.append(mean_accr)
    
#     fold_idx+=1
    
# print("average mae = {}".format(np.average(accrs)))


# #average mae = 0.09419818387885329


# In[32]:


# #data imbalance feature는 삭제하고 다시 시도
# #원래는 down-sampling 또는 up-sampling 또는 배깅 기법을 사용해야함

# import pandas as pd

# datapath = "./toy_dataset.csv"
# data1 = pd.read_csv(datapath,sep=",")
# data1.shape

# import pandas as pd
# import numpy as np
# from sklearn import preprocessing

# le_city = preprocessing.LabelEncoder()
# tmp=["City","Gender","Illness"]
# les={"City":le_city}

# final_data = data1.copy()
# for x in tmp:
#     les[x] = preprocessing.LabelEncoder()
#     final_data[x]=les[x].fit_transform(data1[x])

# from sklearn.model_selection import KFold
# from sklearn.linear_model import LinearRegression

# features = ["Gender","City","Age"]

# kf = KFold(n_splits=10,shuffle=True)
# accrs = []
# fold_idx = 1

# for train_idx,test_idx in kf.split(final_data):
#     print("fold {}".format(fold_idx))
#     train_d, test_d = final_data.iloc[train_idx],final_data.iloc[test_idx]
    
#     train_y = train_d["Income"]
#     train_x = train_d[features]

#     test_y = test_d["Income"]
#     test_x = test_d[features]

#     models = LinearRegression()
#     models.fit(train_x,train_y)
#     print("model {} = {}".format(models,models.score(test_x,test_y)))
    
#     mean_accr = models.score(test_x,test_y)
#     accrs.append(mean_accr)
    
#     fold_idx+=1
    
# print("average mae = {}".format(np.average(accrs)))

# #average mae = 0.09082204234669289
# #데이터 불균형을 이루는 feature를 삭제하니 성능이 좋아짐. 하지만 별 차이가 없음.


# In[36]:


# 여러번의 feature 조정을 통해 최종선택된 feature : "Gender","Age"


import pandas as pd

datapath = "./toy_dataset.csv"
data1 = pd.read_csv(datapath,sep=",")
data1.shape

import pandas as pd
import numpy as np
from sklearn import preprocessing

le_city = preprocessing.LabelEncoder()
tmp=["City","Gender","Illness"]
les={"City":le_city}

final_data = data1.copy()
for x in tmp:
    les[x] = preprocessing.LabelEncoder()
    final_data[x]=les[x].fit_transform(data1[x])

from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression

features = ["Gender","Age"]

kf = KFold(n_splits=10,shuffle=True)
accrs = []
fold_idx = 1
accrs1 = []

for train_idx,test_idx in kf.split(final_data):
    print("fold {}".format(fold_idx))
    train_d, test_d = final_data.iloc[train_idx],final_data.iloc[test_idx]
    
    train_y = train_d["Income"]
    train_x = train_d[features]

    test_y = test_d["Income"]
    test_x = test_d[features]

    models = LinearRegression(normalize=True)
    models.fit(train_x,train_y)
    print("model {} = {}".format(models,models.score(test_x,test_y)))
    print(models.coef_) # 모델 파라미터(계수 부분)
    print(models.intercept_) # 모델 파라미터(상수항 부분)
    
    mean_accr = models.score(test_x,test_y)
    accrs.append(mean_accr)
    mean_accr1 = models.intercept_
    accrs1.append(mean_accr1)
    
    fold_idx+=1
    
print("average mae = {}".format(np.average(accrs)))
print("average intercept = {}".format(np.average(accrs1)))

# average mae = 0.03948946734342723
# average intercept = 85718.4262771987


# In[38]:


import pandas as pd

datapath = "./toy_dataset.csv"
data1 = pd.read_csv(datapath,sep=",")
data1.shape

import pandas as pd
import numpy as np
from sklearn import preprocessing

le_city = preprocessing.LabelEncoder()
tmp=["City","Gender","Illness"]
les={"City":le_city}

final_data = data1.copy()
for x in tmp:
    les[x] = preprocessing.LabelEncoder()
    final_data[x]=les[x].fit_transform(data1[x])

from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression

features = ["Gender","Age"]

kf = KFold(n_splits=10,shuffle=True)
accrs = []
fold_idx = 1

for train_idx,test_idx in kf.split(final_data):
    print("fold {}".format(fold_idx))
    train_d, test_d = final_data.iloc[train_idx],final_data.iloc[test_idx]
    
    train_y = train_d["Income"]
    train_x = train_d[features]

    test_y = test_d["Income"]
    test_x = test_d[features]

    models = LinearRegression(normalize=True)
    models.fit(train_x,train_y)
    models.intercept_ = 85718.4262771987
    print("model {} = {}".format(models,models.score(test_x,test_y)))
    print(models.coef_) # 모델 파라미터(계수 부분)
    print(models.intercept_) # 모델 파라미터(상수항 부분)
    
    mean_accr = models.score(test_x,test_y)
    accrs.append(mean_accr)
    
    fold_idx+=1
    
print("average mae = {}".format(np.average(accrs)))

# average mae = 0.03941547676242012


# In[ ]:


# imbalance data를 처리하기 위해서는 원래 down-sampling 또는 up-sampling 또는 배깅 기법을 사용해야함. 하지만 수업에서 배운 적이 없기 때문에 feature 삭제가 가장 최선인거 같아서, 진행해봤지만 성능에는 큰영향을 미치지 않았다.
# Linaer Regression 모델의 파라미터인 상수항을, 전에 했던 데이터의 상수항들의 평균으로 고정시켜서 모델에 적용했더니 성능이 좋아졌지만 별 차이가 없었다.
# 아마도 Linaer Regression model 내에서 자동으로 최적의 상수항을 조정해주는데, 그거를 평균화 한 값을 넣었기 때문에 좀 더 성능이 좋아지지 않았나 싶다.
# feature를 "Gender","Age"로 선택한 이유는 City는 성능에 큰 악영향을 미치고, Illness는 imbalance하며, Number은 그냥 라벨값이기 때문이다. 이 3개의 feature를 빼게되면 나머지가 2개가 남는데 그 것이 "Gender","Age"이다.

